module.exports={

    //默认服务器端口
    default_port:3000,
    //数据库引擎启动路径
    mongodb_exec_path:"~/workspace/mongodb-osx-x86_64-3.4.4/bin/mongod",
    //数据库位置
    mongodb_db_path:"~/workspace/mongodb-osx-x86_64-3.4.4/bin/data/",
    
}