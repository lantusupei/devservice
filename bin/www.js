let app = require('../app');
let debug = require('debug')('init:server');
let http = require('http');
let mongoose = require("mongoose");
let config = require("../config");
let process = require("process");
let child = require("child_process")

let port =
  process.argv[2] == undefined ? config.default_port : process.argv[2];
port = parseInt(port)

app.set('port', port);
let server = http.createServer(app);

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

//以子进程的方式启动Mongodb
//let db_process = child.spawn(config.mongodb_exec_path,["dbpath",config.mongodb_db_path])


function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  let addr = server.address();
  console.log("Server is runing ", addr);
  mongoose.connect("127.0.0.1", (error) => {
    if (!error) {
      console.log("Connect to database success.")
    } else {
      console.log("Connect to database Faild !")
    }
  })
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
